import process from 'process';
import fs from 'fs';
import gulp from 'gulp';
import sass from 'gulp-sass';
import babel from 'gulp-babel';
import uglify from 'gulp-uglify';
import { default as dart } from 'sass';
import sourcemaps from 'gulp-sourcemaps';
import browserSync from 'browser-sync';
import cleanCSS from 'gulp-clean-css';
import htmlmin from 'gulp-htmlmin';
import replace from 'gulp-replace';
import webpack from 'webpack-stream';
import Fiber from 'fibers';
import del from 'del';

sass.compiler = dart;

const env = process.env.NODE_ENV || 'development';
const isEnvProd = env === 'production';
const dest = isEnvProd
  ? process.env.OUTPUT_DIR
    ? process.env.OUTPUT_DIR
    : 'dist'
  : '_site';

const paths = {
  scripts: {
    srcset: 'src/js/*.js',
    src: 'src/js/main.js',
    dest: `${dest}/assets/js/`,
  },
  styles: {
    srcset: 'src/css/*.scss',
    src: 'src/css/styles.scss',
    dest: `${dest}/assets/css/`,
  },
  layouts: {
    src: 'index.html',
    dest: `${dest}/`,
  },
};

const server = browserSync.create();

export const clean = () => del([dest]);

export const reload = done => {
  server.reload();
  done();
};

export const runServer = done => {
  server.init({
    server: {
      baseDir: dest,
    },
    open: false,
  });
  done();
};

export const scripts = done => {
  gulp
    .src(paths.scripts.src, { sourcemaps: true })
    .pipe(
      webpack({
        mode: env,
        output: {
          filename: 'main.js',
        },
      })
    )
    .on('error', console.error)
    .pipe(babel())
    .pipe(uglify())
    .pipe(gulp.dest(paths.scripts.dest));
  done();
};

export const styles = done => {
  gulp
    .src(paths.styles.src)
    .pipe(sourcemaps.init())
    .pipe(sass({ fiber: Fiber, outputStyle: 'compressed' }))
    .on('error', sass.logError)
    .pipe(cleanCSS())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(paths.styles.dest));
  done();
};

export const layouts = done => {
  if (!isEnvProd) {
    gulp.src(paths.layouts.src).pipe(gulp.dest(paths.layouts.dest));
  } else {
    gulp
      .src(paths.layouts.src)
      .pipe(
        replace(
          /<link rel="stylesheet" href="assets\/css\/styles.css" \/>/,
          () => {
            const style = fs.readFileSync(
              `${paths.styles.dest}styles.css`,
              'utf8'
            );
            return `<style>\n${style.split('\n')[0]}</style>`;
          }
        )
      )
      .pipe(replace(/="\/assets/g, '="assets'))
      .pipe(htmlmin({ collapseWhitespace: true }))
      .pipe(gulp.dest(paths.layouts.dest));
  }
  done();
};

export const debug = done => {
  console.log('env:', env);
  console.log('dest:', dest);
  done();
};

export const watch = done => {
  gulp.watch(paths.styles.srcset, gulp.series(styles));
  gulp.watch(paths.scripts.srcset, gulp.series(scripts));
  gulp.watch(paths.layouts.src, gulp.series(layouts));
  gulp.watch(dest, gulp.series(reload));
  done();
};

export const build = gulp.series(clean, gulp.parallel(styles, scripts));

export const serve = gulp.series(
  clean,
  gulp.parallel(styles, scripts, layouts),
  runServer,
  watch
);

export default serve;

# Singularity

Startpage with almost nothing else but a single input field

---

## Usage

- `ctrl+m` to toggle between modes (current modes: search, address, keyword)
- `ctrl+enter` to open site in new tab
- `f` to focus on input field
- Double tap anywhere (aside from the input field and links) to toggle between
  modes

---

## Goals

- Clean minimal interface
- Ideally a clean codebase (using `webpack-stream` with Gulp for ES module-like
  imports/exports; sorry CJS)
- Accessible to some extent
- Light/dark themes
- Fast page load

## Non-goals

- IE-compatibility

---

## Development

### Getting started

```bash
git clone https://gitlab.com/japorized/singularity.git
cd singularity
npm install
npm run setup:dev
```

### Getting the dev server up

```bash
npm run dev
```

### Testing

```bash
npm run test

# or run individual pieces
npm run prettier
npm run lint
```

### Building for production

```bash
npm run build
```

### Deploying for Gitlab pages

```bash
npm run deploy
```

See also [.gitlab-ci.yml](https://gitlab.com/japorized/singularity/-/blob/master/.gitlab-ci.yml)

---

## Contribution

[Submit an issue](https://gitlab.com/japorized/singularity/-/issues) or
[Create a merge request](https://gitlab.com/japorized/singularity/-/merge_requests)

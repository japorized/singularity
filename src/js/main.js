import Hammer from 'hammerjs';
import { keywords } from './keywords';

const Modes = {
  SEARCH: 'Search',
  ADDRESS: 'Address',
  KEYWORDS: 'Keywords',
};
const modeDisplay = document.querySelector('#mode');
const helpmsg = document.querySelector('#help-msg');
const omnibar = document.querySelector('#omnibar');

let currentMode = Modes.SEARCH;

const setOmnibarPlaceholder = () => {
  switch (currentMode) {
    case Modes.ADDRESS:
      modeDisplay.textContent = Modes.ADDRESS;
      break;
    case Modes.KEYWORDS:
      modeDisplay.textContent = Modes.KEYWORDS;
      break;
    case Modes.SEARCH:
    default:
      modeDisplay.textContent = Modes.SEARCH;
  }
};

const switchToNextMode = () => {
  switch (currentMode) {
    case Modes.SEARCH:
      currentMode = Modes.ADDRESS;
      break;
    case Modes.ADDRESS:
      currentMode = Modes.KEYWORDS;
      break;
    default:
      currentMode = Modes.SEARCH;
  }
  setOmnibarPlaceholder();
};

const handleKeydown = (event) => {
  if (event.ctrlKey && event.keyCode === 77) {
    event.preventDefault();
    helpmsg.textContent = '';
    switchToNextMode();
  } else if (event.keyCode === 13) {
    if (omnibar.value) {
      const url = omnibarDecides();
      if (url) {
        if (event.ctrlKey) {
          const win = window.open(url, '_blank');
          win.focus();
        } else {
          window.location.href = url;
        }
      } else {
        const suggestion = provideKeywordSuggestion(omnibar.value);
        helpmsg.innerHTML = suggestion.join(' • ');
      }
    }
  }
};

const provideKeywordSuggestion = (currentVal) => {
  const keys = Object.keys(keywords);
  const rx = new RegExp(currentVal, 'i');
  const collection = [];
  for (const key of keys) {
    if (key.match(rx)) {
      collection.push(`<a href="${keywords[key]}">${key}</a>`);
    }
  }
  return collection;
};

const handleFocus = () => {
  document.onkeyup = null;
};

const handleBlur = () => {
  document.onkeyup = focusOmnibar;
};

const focusOmnibar = (event) => {
  if (event.keyCode === 70) {
    omnibar.focus();
  }
};

const omnibarDecides = () => {
  const value = omnibar.value;
  switch (currentMode) {
    case Modes.ADDRESS: {
      const httpRegex = new RegExp(/^https?:\/\//);
      return value.match(httpRegex) ? value : `https://${value}`;
    }
    case Modes.KEYWORDS: {
      return keywords[value.toLowerCase()];
    }
    case Modes.SEARCH:
    default:
      return `https://duckduckgo.com/?q=${value}`;
  }
};

let touchManager = null;
const DoubleTap = new Hammer.Tap({
  event: 'doubletap',
  taps: 2,
});

const handleDOMContentLoaded = () => {
  touchManager = new Hammer.Manager(document.body);
  touchManager.add(DoubleTap);
  touchManager.on('doubletap', () => {
    helpmsg.textContent = '';
    switchToNextMode();
  });
  omnibar.addEventListener('focus', handleFocus);
  omnibar.addEventListener('keydown', handleKeydown);
  omnibar.addEventListener('blur', handleBlur);

  document.removeEventListener('DOMContentLoaded', handleDOMContentLoaded);
};

document.addEventListener('DOMContentLoaded', handleDOMContentLoaded);

const unmount = () => {
  touchManager.off('doubletap');
  omnibar.removeEventListener('focus', handleFocus);
  omnibar.removeEventListener('keydown', handleKeydown);
  omnibar.removeEventListener('blur', handleBlur);

  window.removeEventListener('unload', unmount);
};

window.addEventListener('unload', unmount);
